In 2021, we promised our client, [Saleslift Studio][], to deliver a working software solution to improve prospecting performance within a fixed time and at a fixed price. In just 6 weeks (a single development cycle), we provided them with a useful solution - [Lead Studio][]. Further incremental development enabled them to improve a number of key performance indicators. Thanks to their partnership with [Software Garden][], prospecting specialists at Saleslift Studio are now able to enrich leads 20⨉ faster than before.

![Lead Studio](../assets/lead-studio-marquee.jpg)


## The Why

Like many organizations, before our collaboration with Saleslift Studio, their workflow was bottle-necked by an expensive and labor-intensive manual procedure. Prospectors gathered and refined leads by hand, an error-prone, time- and energy-consuming activity.

We sat down together to discuss how Software Garden could provide an automation solution which frees up time, attention and creativity for Saleslift Studio. In this initial phase we identified a number of metrics and key performance indicators. These KPIs currently measure the real value that our software provides for Saleslift Studio:

 * Reducing the time it takes for a prospector to enrich 100 contacts through automation
 
 * Improve the equality of enriched contacts with intelligent algorithms
 
 * Increase the conversion rate of leads to business partners by improving the quality of data
 
 * Provide a great, intuitive user experience to make refining contacts as smooth as possible for prospectors
  
 * A robust security framework that protects Saleslift Studio's data and the data of their clients
 
 * Keeping operational costs as low as possible

![Priorities](../assets/lead-studio-priorities.png)

Agile business analysis is a cornerstone of the value we provide for our clients. We think with our clients about how Software Garden's automation solutions can provide them with maximal value.


## The What

Once we had achieved a high degree of clarity around the kinds of opportunities, problems and constraints faced by our client, we got to work. Based on our industry experience, we were able to quickly design a solution that would advance Saleslift Studio's goals within a fixed period of time and for a fixed budget.

Our solution features: 

 * Self-learning processes that provide better results the more they are used

 * An intuitive UX design to get prospectors up-and-running quickly 

 * An automation solution that respect the established workflows of Saleslift Studio's prospectors

 * A browser extension to automate basic prospecting tasks

 * A Google Sheets add-on to automate data exchange

 * Reliable data exchange via Google Cloud

 * Integration with LinkedIn and Sales Navigator, tools used by Saleslift Studio's prospectors

 * Integration with the Salesloft API

 * Integration with Hunter.io email address search and verification

 * Robust error monitoring with Sentry



## The How

We applied our proven software development method. First we identified the business value and opportunities to capture it, as well as constraints limiting possible solutions.

![Designing](../assets/lead-studio-designing.png)

At Software Garden, we promise our clients that within a single six-week development cycle we will deliver a useful, working software solution that adds real value to their organization. We made this exact promise to Saleslift Studio and delivered on it.

The work involved numerous user interviews and brainstorming sessions, testing different ideas and gathering user feedback. The development of self-learning systems for the enrichment of leads required significant data engineering efforts spearheaded by our colleague Fana Mehari.

Our software solution continues to analyze thousands of lead data records, freeing up prospectors to cultivate relationships with potential clients. Thanks to robust error detection and incidents response people at Saleslift Studio trust that the system will continue bringing value to their business.

[Saleslift Studio]: https://salesliftstudio.com/
[Lead Studio]: https://leadstudio.salesliftstudio.com/
[Software Garden]: https://software.garden/
