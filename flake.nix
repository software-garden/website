{
  description = "Software Garden website: https://software.garden/";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        node-dependencies = (
          pkgs.callPackage ./node-dependencies.nix {}
        ).nodeDependencies.override {
          buildInputs = [
            pkgs.pkg-config
            pkgs.vips
            pkgs.python
            pkgs.nodePackages.node-gyp-build
          ];
        };
        shared-inputs = [
          pkgs.gnumake
          pkgs.nodejs
          pkgs.python
          pkgs.nodePackages.node-gyp-build
          pkgs.envsubst
          pkgs.jq
          pkgs.vips
          pkgs.lynx
          pkgs.gnugrep
          (pkgs.nuspellWithDicts [pkgs.hunspellDicts.en-us-large])
          pkgs.ffmpeg
          pkgs.blender
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "software-garden-website";
          src = self;
          buildInputs = shared-inputs ++ [
            node-dependencies
          ];
          buildPhase = "make";
          installPhase = "make install";
          patchPhase = ''
            ln -s ${node-dependencies}/lib/node_modules ./node_modules
          '';
          hunspell_en_US = pkgs.hunspellDicts.en-us-large;
        };

        devShell = pkgs.mkShell {
          name = defaultPackage.name + "-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.nodePackages.node2nix
            pkgs.miniserve
            pkgs.cachix
            pkgs.miniserve
            pkgs.jq
            pkgs.httpie
          ];
          hunspell_en_US = pkgs.hunspellDicts.en-us-large;
        };

      }
    );
}
