include Makefile.d/defaults.mk

export PATH := ./node_modules/.bin:$(PATH)
entries := \
	src/index.pug \
	src/dashboard.pug \
	src/for-investors.pug \
	src/for-professionals.pug

all: ## Run unit tests and build the program
all: dist
all: spell-check
.PHONY: all


dist: ## Transform the content and store in the dist/ directory.
dist: dependencies
dist: assets
dist: redirects
dist: base-url ?= "/"
dist:
	parcel build \
		--no-optimize\
		--public-url=$(base-url) \
		--no-autoinstall \
		$(entries)
	touch $@

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

dependencies:
	npm clean-install
.PHONY: dependencies

package-lock.json: package.json
	npm install --package-lock-only

redirects: src/redirects src/redirect.html
	bash ./generate-redirects src/redirects
.PHONY: redirects

video-widths := 1080 540 360 270 180
dashboard-videos := $(foreach width, \
	$(video-widths), \
	assets/dashboard-on-fairphone-4-$(width).mp4 \
)

assets: ## Prepare all the assets
assets: $(dashboard-videos)
	touch $@

assets/dashboard-on-fairphone-4-1080.mp4: src/dashboard-on-fairphone-4.blend
	blender $< \
		--background \
		--render-anim

assets/dashboard-on-fairphone-4-%.mp4: assets/dashboard-on-fairphone-4-1080.mp4
	ffmpeg \
	  -i $< \
	  -y \
	  -vf "
	    fps=30,
	    scale=$*:-1,
	    crop=trunc(iw/2)*2:trunc(ih/2)*2
	  " \
	  $@


spell-check: ## Show all misspelled words on the website
spell-check: artifacts/misspelled-words.txt
	bash spell-check.d/spell-check $<
.PHONY: spell-check


artifacts/custom.dic: spell-check.d/custom.dic
	mkdir --parents $(@D)
	cat \
		${hunspell_en_US}/share/hunspell/en_US.dic \
		spell-check.d/custom.dic \
    > $@

artifacts/custom.aff:
	mkdir --parents $(@D)
	cat ${hunspell_en_US}/share/hunspell/en_US.aff \
	> $@

artifacts/misspelled-words.txt: dist
artifacts/misspelled-words.txt: artifacts/custom.dic
artifacts/misspelled-words.txt: artifacts/custom.aff
artifacts/misspelled-words.txt:
	mkdir --parents $(@D)
	bash spell-check.d/misspelled-words \
	| sort \
	| uniq \
	> $@


### DEVELOPMENT

develop: ## Watch, rebuild and serve the content
develop: dependencies
develop: redirects
develop: assets
develop:
	parcel $(entries)
.PHONY: develop

serve: ## Serve the built program
serve: dist
serve:
	miniserve --index index.html --port 1234 dist/
.PHONY: serve

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

help: ## Print this help message (DEFAULT)
help: # TODO: Handle section headers in awk script
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden
node-nix-expressions := node-packages.nix node-dependencies.nix node-env.nix node-supplement.nix

result: ## Build the program using Nix
result: $(node-nix-expressions)
result:
	# FIXME: The result target is currently broken.
	# Probably we need to add some stuff to node-supplement.json
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

serve-result: ## Serve the program built using Nix
serve-result: result
	miniserve --index index.html --port 1234 result/
.PHONY: serve-result

$(node-nix-expressions): package-lock.json node-supplement.json
	rm -rf node_modules
	node2nix \
		--development \
		--lock package-lock.json \
		--composition $@ \
		--supplement-input node-supplement.json \
		--supplement-output node-supplement.nix
